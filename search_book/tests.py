from django.test import TestCase, Client
from django.urls import resolve

from .views import index

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

import time

# Create your tests here.
class UnitTest(TestCase):
    def test_if_url_exist(self):
        response = Client().get("/search_book/")
        self.assertEqual(response.status_code, 200)

    def test_if_url_exist_using_func(self):
        found = resolve('/search_book/')
        self.assertEqual(found.func, index)

    def test_if_index_used_the_right_template(self):
        response = Client().get('/search_book/')
        self.assertTemplateUsed(response, 'search.html')

    def test_if_page_does_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)

    def test_url_book_api_ajax(self):
        response = Client().get("/book_api/")
        self.assertEqual(response.status_code, 200)