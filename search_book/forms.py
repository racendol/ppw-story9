from django import forms

class SearchForm(forms.Form):
    attrs = {
        'type':'text',
        'class': 'form-control',
        'id':'search-input'
    }

    text_input = forms.CharField(label='Cari buku yang ingin kamu tahu!', required=True, widget=forms.TextInput(attrs=attrs))
