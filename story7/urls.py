"""story7 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from mainapp.views import index
from search_book.views import index as search_index
from search_book.views import book_search
from django.contrib.auth import views as auth_views
from login_page.forms import CustomAuthForm

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name = 'index'),
    path('search_book/', search_index, name = 'search_index'),
    path('book_api/', book_search, name = 'book_search'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html', authentication_form=CustomAuthForm), name = 'login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='logout.html', next_page='/'), name = 'logout')

]
