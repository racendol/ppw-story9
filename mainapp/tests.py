from django.test import TestCase, Client
from django.urls import resolve

from .views import index

# Create your tests here.
class UnitTest(TestCase):
    def test_if_url_exist(self):
        response = Client().get("/")
        self.assertEqual(response.status_code, 200)

    def test_if_url_exist_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_if_index_used_the_right_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_if_page_does_not_exist(self):
        response = Client().get('/NotExistPage/')
        self.assertEqual(response.status_code, 404)

