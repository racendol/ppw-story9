from django.test import TestCase, Client
from django.urls import resolve

# Create your tests here.
class UnitTest(TestCase):
    def test_if_url_exist(self):
        response = Client().get("/login/")
        self.assertEqual(response.status_code, 200)

    def test_if_index_used_the_right_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')
        